package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
        //ToDo: Complete me
        public DefendWithShield(){ }

        public String defend(){
            return "Defend with Shield";
        }

        public String getType(){ return "DefendWithShield";}
}
