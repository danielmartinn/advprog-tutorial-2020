package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                //ToDo: Complete Me
                this.guild = guild;
        }

        //ToDo: Complete Me
        public void update(){
                if (guild.getQuestType().equals("D")) {
                        this.getQuests().add(guild.getQuest());
                }
                else if (guild.getQuestType().equals("E")){
                        this.getQuests().add(guild.getQuest());
                }
        }
}
