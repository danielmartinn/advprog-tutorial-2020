package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
        //ToDo: Complete me
        public String getAlias(){
            return "Agile";
        }
        public AgileAdventurer(){
            this.setAttackBehavior(new AttackWithGun());
            this.setDefenseBehavior(new DefendWithBarrier());
        }
}
