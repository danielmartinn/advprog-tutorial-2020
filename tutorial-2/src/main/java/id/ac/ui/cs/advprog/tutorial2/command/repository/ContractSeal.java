package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {
    private Spell latestSpell;
    private boolean booleanUndo;

    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        spells.get(spellName).cast();
        this.latestSpell = spells.get(spellName);
        this.booleanUndo = false;
    }

    public void undoSpell() {
        // TODO: Complete Me
        if (!booleanUndo){
            this.latestSpell.undo();
            booleanUndo = true;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
