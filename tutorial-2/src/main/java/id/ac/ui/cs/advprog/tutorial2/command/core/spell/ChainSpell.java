package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected ArrayList<Spell> chainSpellList;

    public ChainSpell(ArrayList<Spell> chainSpellList){
        this.chainSpellList = chainSpellList;
    }

    public void cast(){
        for(Spell spell : chainSpellList) {
            spell.cast();
        }
    }

    public void undo(){
        for(int i = this.chainSpellList.size()-1; i >= 0; i--){
            chainSpellList.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
